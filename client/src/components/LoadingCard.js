import React, { Component } from 'react'
import { Card } from "@shopify/polaris"

export default class LoadingCard extends Component {
  // set initial state
  state = {
    isLoading: true
  }

  componentWillMount = () => {
    // mock behaviour of ajax requests or promise
    setTimeout(() => {
      this.setState({
        isLoading: false
      })
    }, 2000);
  }
  
  // actual method that renders
  render() {
    return (
      <Card title="Loading Card Example" sectioned>
        { this.state.isLoading ? (
          <p>Card is currently loading...</p>
        ):(
          <p> Card has finished loading!!! </p>
        )}
      </Card>
    )
  }
}
