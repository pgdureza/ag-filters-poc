import React, { Component } from 'react';
import './App.css';
import LoadingCard from './components/LoadingCard';
import Header from './components/Header';
import { Layout, Page, AppProvider } from "@shopify/polaris"

class App extends Component {
  render() {
    return (
      <AppProvider>
      <div className="App">
        <Header />
        <Page >
          <Layout>
            <Layout.Section>
              
                <LoadingCard/>
            </Layout.Section>
          </Layout>
        </Page>
      </div>
      </AppProvider>
    );
  }
}

export default App;
