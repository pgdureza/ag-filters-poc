
require('dotenv').config();
const express = require('express');
const chalk = require('chalk');
const path = require('path');
const staticPath = path.resolve(__dirname, '../client/build');
const http = require('http');
const ShopifyExpress = require('@shopify/shopify-express');
const session = require('express-session');
const app = express();
const fs = require('fs');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


const {
  SHOPIFY_APP_KEY,
  SHOPIFY_APP_HOST,
  SHOPIFY_APP_SECRET,
  NODE_ENV,
} = process.env;
// const SHOPIFY_APP_HOST = 'https://50fd7c14.ngrok.io'

app.use(
  session({
    store: undefined, // DON'T USE REDIS isDevelopment ? undefined : new RedisStore(),
    secret: SHOPIFY_APP_SECRET,
    resave: true,
    saveUninitialized: false,
  })
);

app.use('/assets', express.static(staticPath));
app.get('/install', (req, res) => res.render('install'));

const server = http.createServer(app);
const port = process.env.PORT || 3000;

// console.log('SHOPIFY_APP_HOST', SHOPIFY_APP_HOST)
const shopifyConfig = {
  host: SHOPIFY_APP_HOST,
  apiKey: SHOPIFY_APP_KEY,
  secret: SHOPIFY_APP_SECRET,
  scope: ['write_content'],
  afterAuth(request, response) {
    return response.redirect('/');
  },
};

const shopify = ShopifyExpress(shopifyConfig);

// Mount Shopify Routes
const {routes, middleware} = shopify;
const {withShop, withWebhook} = middleware;

app.use('/shopify', routes);

// get hash for js
let jsHash;
fs.readdirSync(staticPath + "/static/js").forEach(file => {
  jsHash = file.split('.')[1];
})

// get hash for css
let cssHash;
fs.readdirSync(staticPath + "/static/css").forEach(file => {
  cssHash = file.split('.')[1];
})

// Client
app.get('/', withShop({authBaseUrl: '/shopify'}), function(request, response) {
  const { session: { shop, accessToken } } = request;
  response.render('app', {
    title: 'AG Form Builder',
    apiKey: shopifyConfig.apiKey,
    shop: shop,
    jsHash: jsHash,
    cssHash: cssHash
  });
});

server.listen(port, err => {
  if (err) {
    return console.log('😫', chalk.red(err));
  }
  console.log(`🚀 Now listening on port ${chalk.green(port)}`);
});

