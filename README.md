This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

This project serves as a boilerplate for creating simple shopify apps built with react and express.

Local Setup

#ROOT
1. rename .env.sample to .env and enter necessary data
2. do the same for client/.env.sample

React Development
run this to start react app
`
yarn client:start
`